<?php
// $Id: $

/**
 * @file
 * This module allows importing members of an LDAP group as users.
 *
 * THIS MODULE IS UNDER DEVELOPMENT AND NOT YET READY FOR PRODUCTION RELEASE.
 *
 * This module was written to work with Novell eDirectory and queries
 * the groupMembership property.
 *
 * @ingroup hfcc_modules
 */

/**
 * Implementation of hook_init().
 */
function ldap_group_import_init() {
  require_once(drupal_get_path('module', 'ldapauth') .'/includes/LDAPInterface.inc');
  require_once(drupal_get_path('module', 'ldapgroups') .'/ldapgroups.inc');
}

/**
 * Implementation of hook_menu().
 */
function ldap_group_import_menu() {
  $items['admin/settings/ldap/group-import'] = array(
    'title' => t('Group Import'),
    'description' => t('Import members of a specified LDAP group.'),
    'page callback' => '_ldap_group_import_admin',
    'access arguments' => array('administer ldap modules'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Generate an administration page for LDAP Group Import.
 *
 * @todo: This could probably just be dropped in favor of adding markup elements
 * to the form, but it will do for now.
 */
function _ldap_group_import_admin() {
  $output = '<div class="content clear-block">';
  $output .= '<p>At some point, we should probably add some instructions here.</p>';
  $output .= drupal_get_form('_ldap_group_import_form');
  $output .= '</div>';
  return $output;
}

/**
 * Specifications for the LDAP Group Import Form.
 *
 * Note that since subsequent submits kick back to the form, this is where
 * all of the processing happens, too. See the todo item below for how we
 * probably need to fix the redirect and response actions when users are added.
 */
function _ldap_group_import_form($form_state) {
  // echo '<pre style="background: #fff;">'. print_r($form_state, 1) .'</pre>';
  $groupname = $form_state['post']['group'];
  $form['group'] = array(
    '#type' => 'textfield',
    '#title' => t('Group Name'),
    '#size' => 50,
    '#maxlength' => 100,
    '#required' => TRUE,
  );
  /**
   * If user names exist and are checked, add them as users.
   * Do this before searching for available users so the new
   * entries will be filtered from the list.
   */
  if (is_array($form_state['post']['matches'])) {
    _ldap_group_import_process($form_state['post']['matches']);
    /**
     * @todo: Find a better solution for this. The problem is that when we create
     * the user, it generates an ugly "Illegal choice" error because we won't display
     * the newly created user in the "Matching Users" checkboxes below, so Drupal
     * freaks out because it was checked on the way in. Gotta be a way to supress that,
     * but for now, redirect to users page.
     */
    drupal_goto('admin/user/user');
  }
  /**
   * If a groupname is set, display available users.
   */
  if ($groupname !== '') {
    $matches = _ldap_group_import_search($groupname);
    if ($matches) {
      $options = array();
      foreach ($matches as $match) {
        $cn = $match['cn'];
        $options[$cn] = $match['fullname'] .' <em>('. $match['dn'] .')</em>';
      }
      asort($options);
      $form['matches'] = array(
        '#type' => 'checkboxes',
        '#title' => 'Matching Users',
        '#options' => $options,
      );
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Load'),
  );
  $form['#redirect'] = FALSE;
  return $form;
}

/**
 * Wrapper function for the LDAP lookup. Cycles through available LDAP configurations.
 *
 * @param $groupname
 *   A fully-qualified LDAP group name.
 */
function _ldap_group_import_search($groupname) {
  $matches = '';
  // Cycle through LDAP configurations.  First one to succeed wins.
  $result = db_query("SELECT sid FROM {ldapauth} WHERE status = 1 ORDER BY weight");
  while ($row = db_fetch_object($result)) {

    // Initialize LDAP.
    if (!_ldapauth_init($row->sid))
      return FALSE;

    // Look up the user in LDAP.
    $entries = _ldap_group_import_lookup($groupname);
    if ($entries) {
      foreach ($entries as $entry) {
        // $matches .= '<li title="'. $entry['dn'] .'">'. $entry['fullname'] .' ('. $entry['cn'] .')</li>';
        $matches[] = $entry;
      }
    }
  }
  return $matches;
}

/**
 * Queries LDAP server for the users.
 * 
 * This was adapted from _ldapauth_user_lookup in ldapauth.module.
 *
 * @param $groupname
 *   A fully-qualified LDAP group name.
 *
 * @return
 *   An array with matching user data or NULL if not found.
 */
function _ldap_group_import_lookup($groupname) {
  global $_ldapauth_ldap;
  
  if (!$_ldapauth_ldap)
    return;

  // If there is no bindn and bindpw - the connect will be an anonymous connect.
  $_ldapauth_ldap->connect($_ldapauth_ldap->getOption('binddn'), $_ldapauth_ldap->getOption('bindpw'));
  foreach (explode("\r\n", $_ldapauth_ldap->getOption('basedn')) as $base_dn) {
    if (empty($base_dn))
      continue;

    // Normal LDAP users would filter on groupMembership, but we use custom attributes.
    // The hfccgroups and hfccdrupalroles attributes define group membership in our auth directory.
    // $filter = "(|(groupMembership=". $groupname ."))";
    $filter = "(|(hfccgroups=". $groupname .")(hfccdrupalroles=". $groupname ."))";
    $result = $_ldapauth_ldap->search($base_dn, $filter);
    if ($result) {
      $rows = array();
      foreach ($result as $entry) {
        if (is_array($entry)) {
          $row = array();
          $row['dn'] = trim($entry['dn']);
          $row['cn'] = trim($entry['cn'][0]);
          $row['fullname'] = trim($entry['fullname'][0]);
          $rows[] = $row;
        }
      }
      return $rows;
    }
  }
}

/**
 * Process the array of new users to be added.
 *
 * @param $matches
 *   Array of user names to add.
 */
function _ldap_group_import_process($matches) {
  foreach ($matches as $match) {
    if (_ldap_group_import_add($match)) {
      drupal_set_message('Added '. $match);
    }
    else {
      drupal_set_message('Failed to add '. $match, 'error');
    }
  }
}

/**
 * Add a Drupal user from LDAP data.
 *
 * This routine will find the specified user in the LDAP server
 * and add a Drupal account. It will then attempt to find valid
 * mapped groups in LDAP and add the appropriate Drupal role(s),
 * if any.
 *
 * @param $name
 *   Username of user to add.
 *
 * @return
 *   TRUE or FALSE based on success.
 *
 * @todo: Add LDAP Data read to import Full Name on build.
 */
function _ldap_group_import_add($name) {
  global $_ldapauth_ldap;
  $result = db_query("SELECT sid FROM {ldapauth} WHERE status = 1 ORDER BY weight");
  while ($row = db_fetch_object($result)) {

    // Initialize LDAP.
    if (!_ldapauth_init($row->sid))
      return FALSE;

    // Look up the user in LDAP.
    if ($ldap_user = _ldapauth_user_lookup($name)) {
      // If mail attribute is missing, set the name as mail.
      $init = $mail = key_exists(($_ldapauth_ldap->getOption('mail_attr') ? $_ldapauth_ldap->getOption('mail_attr') : LDAPAUTH_DEFAULT_MAIL_ATTR), $ldap_user) ? $ldap_user[$_ldapauth_ldap->getOption('mail_attr')][0] : $name;
      $pass_new = user_password(20);
      $userinfo = array(
        'name' => $name,
        'pass' => $pass_new,
        'mail' => $mail,
        'init' => $init,
        'status' => 1,
        'authname_ldapauth' => $name,
        'ldap_authentified' => TRUE,
        'ldap_dn' => $ldap_user['dn'],
        'ldap_config' => $_ldapauth_ldap->getOption('sid')
      );
      $user = user_save('', $userinfo);
      watchdog('ldap_group_import', 'New external user %name created from the LDAP server %server.', array('%name' => $name, '%server' => $_ldapauth_ldap->getOption('name')), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $user->uid .'/edit'));
      
      // Now try adding ldapgroup information to the user we just created.
      // this is taken from ldapgroups_user_login() in the ldapgroups.inc file.
      
      if (!_ldapgroups_ldap_init($user)) {
        // Return TRUE on the user creation anyway,
        // even if group initialization fails.
        return TRUE;
      }

      $groups = _ldapgroups_detect_groups($user);
      if ($groups === FALSE) {
        return TRUE;
      }
      $groups = _ldapgroups_filter($user, $groups);

      // At this point, the roles are in the full DN format.
      $roles = array();
      if (!empty($groups)) {
        $ldapgroups_mappings = _ldapgroups_ldap_info($user, 'ldapgroups_mappings');
        foreach ($groups as $group) {
          $role = _ldapgroups_mapping($user, $group);
          _ldapgroups_create_role($role);
          _ldapgroups_grant_role($user, $role);
          $roles[] = $role;
        }
      }

      // Store roles in the user object so we know which ones
      // were granted here.
      user_save($user, array('ldap_drupal_roles' => $roles));
      return TRUE;
    }
  }
  // User not found in LDAP.
  return FALSE;
}

/**
 * Alter LDAP Group Import form to remove existing Drupal users from the list.
 */
function ldap_group_import_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == '_ldap_group_import_form') {
    if (is_array($form['matches'])) {
      foreach ($form['matches']['#options'] as $key => $value) {
        // Filter out existing users.
        $result = db_query("SELECT uid FROM {users} WHERE LOWER(name) = LOWER('%s')", $key);
        if (($account = db_fetch_object($result))) {
          unset($form['matches']['#options'][$key]);
        }
      }
    }
  }
}
